from django.db import models
from django.utils.timezone import now, timedelta
# from django_countries.fields import CountryField
from django.utils.translation import ugettext_lazy as _

def get_default_expire_date():
    return now() + timedelta(days=365)

class BrowserField(models.CharField):
    system_check_deprecated_details = {'msg': ('Currently unused field')}
    description = _('Browser')

    CHROME  = '0'
    FIREFOX = '1'
    IE      = '2'
    OPERA   = '3'
    SAFARI  = '4'
    OTHER   = '5'
    UNKNOWN = '6'
    BROWSERS = (
        (CHROME, 'Chrome'),
        (FIREFOX, 'Firefox'),
        (IE, 'Internet Explorer'),
        (OPERA, 'Opera'),
        (SAFARI, 'Safari'),
        (OTHER, 'Other'),
        (UNKNOWN, 'Unknown'),
    )

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 1
        kwargs['choices'] = BrowserField.BROWSERS
        super(BrowserField, self).__init__(*args, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super(BrowserField, self).deconstruct()
        del kwargs['max_length'], kwargs['choices']
        return name, path, args, kwargs


class PlatformField(models.CharField):
    system_check_deprecated_details = {'msg': ('Currently unused field')}
    description = _('Platform')

    LINUX = '0'
    WINDOWS = '1'
    MACINTOSH = '2'
    OTHER = '3'
    UNKNOWN = '4'
    PLATFORMS = (
        (LINUX, 'Linux'),
        (WINDOWS, 'Windows'),
        (MACINTOSH, 'Macintosh'),
        (OTHER, 'Other'),
        (UNKNOWN, 'Unknown'),
    )

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 1
        kwargs['choices'] = PlatformField.PLATFORMS
        super(PlatformField, self).__init__(*args, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super(PlatformField, self).deconstruct()
        del kwargs['max_length'], kwargs['choices']
        return name, path, args, kwargs

class WebPage(models.Model):
    id = models.AutoField(primary_key=True)
    url = models.TextField()
    url_key = models.TextField(blank=True)
    creation_date = models.DateTimeField(default=now)
    expire_date = models.DateTimeField(default=get_default_expire_date)

    def __str__(self):
        return '%s %s %s %s %s' % (self.id, self.url, self.url_key,
                                   self.creation_date, self.expire_date)

# class Click(models.Model):
#     id = models.AutoField(primary_key=True)
#     browser = BrowserField(default=BrowserField.UNKNOWN)
#     country = CountryField()
#     platform = PlatformField(default=PlatformField.UNKNOWN)
#     web_page = models.ForeignKey(WebPage)
#
#     def __str__(self):
#         return '%s %s %s %s' % (self.id, self.browser, self.country, self.platform)
