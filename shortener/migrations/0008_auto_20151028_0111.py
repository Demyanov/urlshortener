# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shortener', '0007_auto_20151027_1929'),
    ]

    operations = [
        migrations.RenameField(
            model_name='webpage',
            old_name='long_url',
            new_name='url',
        ),
        migrations.RenameField(
            model_name='webpage',
            old_name='short_url',
            new_name='url_key',
        ),
    ]
