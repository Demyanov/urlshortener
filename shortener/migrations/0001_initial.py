# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='LongUrl',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.CharField(unique=True, max_length=2083)),
            ],
        ),
        migrations.CreateModel(
            name='ShortUrl',
            fields=[
                ('long_url', models.OneToOneField(primary_key=True, serialize=False, to='shortener.LongUrl')),
                ('url', models.CharField(unique=True, max_length=2083)),
            ],
        ),
    ]
