# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shortener', '0006_auto_20151027_1925'),
    ]

    operations = [
        migrations.AlterField(
            model_name='webpage',
            name='short_url',
            field=models.TextField(blank=True),
        ),
    ]
