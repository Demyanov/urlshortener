# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('shortener', '0004_auto_20151025_0143'),
    ]

    operations = [
        migrations.AlterField(
            model_name='webpage',
            name='creation_date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AlterField(
            model_name='webpage',
            name='long_url',
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name='webpage',
            name='short_url',
            field=models.TextField(unique=True),
        ),
    ]
