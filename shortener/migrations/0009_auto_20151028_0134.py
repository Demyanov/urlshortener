# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shortener', '0008_auto_20151028_0111'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='click',
            name='web_page',
        ),
        migrations.DeleteModel(
            name='Click',
        ),
    ]
