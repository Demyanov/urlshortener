# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import shortener.models
import django_countries.fields
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('shortener', '0003_auto_20151024_0035'),
    ]

    operations = [
        migrations.CreateModel(
            name='Click',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('browser', shortener.models.BrowserField(default='6')),
                ('country', django_countries.fields.CountryField(max_length=2)),
                ('platform', shortener.models.PlatformField(default='4')),
            ],
        ),
        migrations.CreateModel(
            name='WebPage',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('long_url', models.URLField(max_length=2083, unique=True)),
                ('short_url', models.URLField(max_length=2083, unique=True)),
                ('creation_date', models.DateTimeField(default=datetime.datetime.now)),
                ('expire_date', models.DateTimeField(default=shortener.models.get_default_expire_date)),
            ],
        ),
        migrations.RemoveField(
            model_name='shorturl',
            name='long_url',
        ),
        migrations.DeleteModel(
            name='LongUrl',
        ),
        migrations.DeleteModel(
            name='ShortUrl',
        ),
        migrations.AddField(
            model_name='click',
            name='web_page',
            field=models.ForeignKey(to='shortener.WebPage'),
        ),
    ]
