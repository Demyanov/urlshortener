# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shortener', '0002_auto_20151024_0029'),
    ]

    operations = [
        migrations.AlterField(
            model_name='longurl',
            name='url',
            field=models.CharField(unique=True, max_length=2083),
        ),
        migrations.AlterField(
            model_name='shorturl',
            name='url',
            field=models.CharField(unique=True, max_length=2083),
        ),
    ]
