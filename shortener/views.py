from django.shortcuts import render, redirect, get_object_or_404
from django.core.urlresolvers import reverse
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from django.contrib import messages
from django.conf import settings
from .models import WebPage
from shortener.base_62_converter import saturate, dehydrate

def index(request):
    if request.method == 'POST':
        url = request.POST['url-input']

        # TODO: probably should validate on frontend side in html form
        try:
            URLValidator().__call__(url)
        except ValidationError:
            messages.add_message(request, messages.ERROR, 'Enter a valid URL')
        else:
            # TODO: optimize database calls
            web_page = WebPage(url=url)
            web_page.save()
            web_page.url_key = dehydrate(web_page.id)
            web_page.save()

            short_url = request.build_absolute_uri() + web_page.url_key
            messages.add_message(request, messages.INFO, short_url)
        return redirect(reverse('shortener:index'))

    return render(request, 'shortener/index.html')

def expand(request, url_key):
    return redirect(get_object_or_404(WebPage, pk=saturate(url_key)).url)
